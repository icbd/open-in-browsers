import * as vscode from 'vscode';
import * as LiveServer from 'live-server';

export class App {
    constructor() {
    }

    StartServer() {
        var params = {
            port: 62310,
            root: vscode.window.activeTextEditor?.document.uri.fsPath,
            open: true,
        };
        LiveServer.start(params);
        vscode.window.showInformationMessage(`Start live-server on port: ${params.port}`);
    }

    StopServer() {
        LiveServer.shutdown();
        vscode.window.showInformationMessage('Shutdown live-server');
    }

    dispose() {
        this.StopServer();
    }
}
