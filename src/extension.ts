import * as vscode from 'vscode';
import { App } from './App';

export function activate(context: vscode.ExtensionContext) {
	const app = new App();

	context.subscriptions.push(
		vscode.commands.registerCommand('open-in-browsers.OpenInBrowsersStartServer', () => {
			app.StartServer();
		})
	);

	context.subscriptions.push(
		vscode.commands.registerCommand('open-in-browsers.OpenInBrowsersStopServer', () => {
			app.StopServer();
		})
	);

	context.subscriptions.push(app);
}

// This method is called when your extension is deactivated
export function deactivate() { }
